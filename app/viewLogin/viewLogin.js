'use strict';

angular.module('myApp.viewLogin', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewLogin', {
            templateUrl: 'viewLogin/viewLogin.html',
            controller: 'ViewLoginCtrl'
        });
    }])
    .controller('ViewLoginCtrl', ['$http', 'AuthService', function ($http, AuthService) {
        var URL = 'http://localhost:8080';
        var self = this;
        this.formUser = {
            'login': '',
            'password': ''
        };
        this.authenticate = function () {
            $http.post(URL + "/authenticate", self.formUser)
                .then(function (resp) {
                        console.log("Success: " + resp);
                        var token = resp.data.token;
                        var loggedInUser = resp.data.user;

                        $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
                        AuthService.loggedInUser = loggedInUser;
                        window.location = "#!/";
                    },
                    function (resp) {
                        console.log("Error: " + resp);
                    });


        };

    }]);
