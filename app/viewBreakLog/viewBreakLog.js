'use strict';
angular.module('myApp.viewBreakLog', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewBreakLog', {
            templateUrl: 'viewBreakLog/viewBreakLog.html',
            controller: 'ViewBreakLogCtrl'
        });
    }])

    .controller('ViewBreakLogCtrl', ['$http', '$scope', '$rootScope', function ($http, $scope, $rootScope) {
        var URL = 'http://localhost:8080';
        var self = this;
        this.longUserList = [];
        this.shortUserList = [];
        this.loggedInUser = $rootScope.loggedInUser;

        window.setInterval(function () {
            self.updateBreaks();
        }, 1000);

        this.availableShorts = 0;
        this.availableLongs = 0;
        this.user = {
            'email': '',
            'id': '',
            'login': ''
        };

        this.fillLongBreaks = function () {
            self.longUserList = [];
            $http.get(URL + '/queue/long')
                .then(function (data) {
                    self.availableLongs = data.data.availableBreaks;
                }, function () {
                    console.log('error get long');
                });
        };

        this.updateBreaks = function () {
            $http.get(URL + "/breakTime/longs").then(
                function (response) {
                    var array = response.data;
                    self.longUserList = [];
                    for (var index in array) {
                        self.longUserList.push(array[index]);
                    }
                }, function (errResp) {
                    console.log("Error: " + errResp);
                }
            );
            $http.get(URL + "/breakTime/shorts").then(
                function (response) {
                    var array = response.data;
                    self.shortUserList = [];
                    for (var index in array) {
                        self.shortUserList.push(array[index]);
                    }
                }, function (errResp) {
                    console.log("Error: " + errResp);
                }
            );
        };

        this.getNumber = function (num) {
            return new Array(num);
        };

        this.fillShortBreaks = function () {
            self.shortUserList = [];
            $http.get(URL + '/queue/short')
                .then(function (data) {
                    console.log(data);
                    self.availableShorts = data.data.availableBreaks;
                }, function () {
                    console.log('error get short');
                });
        };

        this.takeAShortBreak = function () {
            $http.get(URL + '/queue/takeShortBreak?userid=' + self.loggedInUser)
                .then(function (data) {
                    console.log(data);
                    self.fillShortBreaks();
                }, function () {
                    console.log('error take short break');
                })
        };

        this.takeALongBreak = function () {
            $http.get(URL + '/queue/takeLongBreak?userid=' + self.loggedInUser)
                .then(function () {
                    self.fillLongBreaks();
                }, function () {
                    console.log('error take long break');
                })
        };

        this.removeAShortBreak = function (login) {
            console.log("Check");
            var removedUserId = -1;
            for (var index in self.shortUserList) {
                if (self.shortUserList[index].login === login) {
                    removedUserId = self.shortUserList[index].userId;
                    break;
                }
            }
            if (removedUserId === self.loggedInUser) {
                $http.get(URL + '/queue/removeToShortQueue?userid=' + self.loggedInUser)
                    .then(function (data) {
                            console.log(data);
                            self.fillLongBreaks();
                            self.fillShortBreaks();
                        }, function () {
                            console.log('error give back a short break');
                        }
                    );
            } else {
                blurt('Uwaga!', 'Czy to Ty?');
            }
        };

        this.removeALongBreak = function (login) {
            console.log("Check");
            var removedUserId = -1;
            for (var index in self.shortUserList) {
                if (self.longUserList[index].login === login) {
                    removedUserId = self.longUserList[index].userId;
                    break;
                }
            }
            if (removedUserId !== self.loggedInUser) {
                blurt({'type': 'error'});
                blurt('To nie Ty!');

            } else {
                $http.get(URL + '/queue/removeToLongQueue?userid=' + self.loggedInUser)
                    .then(function (data) {
                            console.log(data);
                            self.fillLongBreaks();
                            self.fillLongBreaks();
                        }, function () {
                            console.log('error give back a short break');
                        }
                    );
            }
        };

        self.updateBreaks();
        self.fillLongBreaks();
        self.fillShortBreaks();

    }]);